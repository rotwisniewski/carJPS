<%--
  Created by IntelliJ IDEA.
  User: rt
  Date: 18.04.2018
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ include file="./common.jsp" %>
<html>
<head>
    <title>Dodaj wycieczkę</title>
</head>
<body>
<h1>Dodaj wycieczkę</h1>
<form:form method="POST" modelAttribute="trip">
    <table>
        <tr>
            Nazwa wycieczki: <form:input path="trip_name"/>
        </tr>
        <tr>
            Początek wycieczki: <form:input path="trip_start_date"/>
        </tr>
        <tr>
            Koniec wycieczki: <form:input path="trip_end_date"/>
        </tr>
        <tr>
            Typ transportu: <form:input path="transport_type"/>
        </tr>

        <tr>
            Kosz transportu: <form:input path="transport_price"/>
        </tr>

        <tr>
            Budżet wycieczki: <form:input path="trip_budget"/>
        </tr>

        <tr>
            Dzienny budżet: <form:input path="daily_budget"/>
        </tr>

      

        <tr>
            <td><input type="submit" value="Zapisz wycieczkę"/></td>
        </tr>
    </table>
</form:form>
</body>
</html>
