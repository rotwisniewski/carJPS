<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <%--Bootstrap--%>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">


</head>
<body>
<!-- multistep form -->
<form id="msform">
    <!-- progressbar -->
    <ul id="progressbar">
        <li class="active">Cel podróży</li>
        <li>Termin</li>
        <li>Budżet</li>
    </ul>
    <!-- fieldsets -->
    <fieldset>
        <h2 class="fs-title">Gdzie jedziemy?</h2>
        <h3 class="fs-subtitle">Wybierz cel podróży</h3>
        <input type="text" name="email" placeholder="Dokąd się wybierasz?" />

        <h3 class="fs-subtitle">Wybierz środek transportu</h3>
        <label class="checkbox-inline"><input type="checkbox" value="">Samochód</label>
        <label class="checkbox-inline"><input type="checkbox" value="">Samolot</label>
        <label class="checkbox-inline"><input type="checkbox" value="">Pociąg</label>

        <!--     <input type="password" name="pass" placeholder="Password" />
            <input type="password" name="cpass" placeholder="Confirm Password" />-->
        <input type="button" name="next" class="next action-button" value="Next" />
    </fieldset>
    <fieldset>
        <h2 class="fs-title">Kiedy jedziemy?</h2>
        <h3 class="fs-subtitle">Wybierz termin podróży</h3>
        <input type="date" name="start_date" placeholder="Od" />
        <input type="date" name="end_date" placeholder="Do" />
        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="button" name="next" class="next action-button" value="Next" />
    </fieldset>
    <fieldset>
        <h2 class="fs-title">Ile chcesz wydać?</h2>
        <h3 class="fs-subtitle">Zaplanuj budżet podróży</h3>
        <input type="number" name="budget" placeholder="Twój budżet" />

        <input type="button" name="previous" class="previous action-button" value="Previous" />
        <input type="submit" name="submit" class="submit action-button" value="Submit" />
    </fieldset>
</form>
</body>