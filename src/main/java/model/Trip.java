package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
@Table(name = "trip")
public class Trip {

    @Id
    @GeneratedValue
    @Column(name="trip_id")
    private Long id;

    private String trip_name;

    private LocalDate trip_start_date;

    private LocalDate trip_end_date;

    private String transport_type;

    private double transport_price;

    private double trip_budget;

    private double daily_budget;



    @OneToMany(mappedBy = "trip_d")
    private List<TouristAttraction> touristAttractions;


    // pusty konstruktor, gettery, settery


    public Trip() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrip_name() {
        return trip_name;
    }

    public void setTrip_name(String trip_name) {
        this.trip_name = trip_name;
    }

    public LocalDate getTrip_start_date() {
        return trip_start_date;
    }

    public void setTrip_start_date(LocalDate trip_start_date) {
        this.trip_start_date = trip_start_date;
    }

    public LocalDate getTrip_end_date() {
        return trip_end_date;
    }

    public void setTrip_end_date(LocalDate trip_end_date) {
        this.trip_end_date = trip_end_date;
    }

    public String getTransport_type() {
        return transport_type;
    }

    public void setTransport_type(String transport_type) {
        this.transport_type = transport_type;
    }

    public double getTransport_price() {
        return transport_price;
    }

    public void setTransport_price(double transport_price) {
        this.transport_price = transport_price;
    }

    public double getTrip_budget() {
        return trip_budget;
    }

    public void setTrip_budget(double trip_budget) {
        this.trip_budget = trip_budget;
    }

    public double getDaily_budget() {
        return daily_budget;
    }

    public void setDaily_budget(double daily_budget) {
        this.daily_budget = daily_budget;
    }

    public List<TouristAttraction> getTouristAttractions() {
        return touristAttractions;
    }

    public void setTouristAttractions(List<TouristAttraction> touristAttractions) {
        this.touristAttractions = touristAttractions;
    }
}
