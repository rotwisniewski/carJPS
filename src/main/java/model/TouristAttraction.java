package model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
@Table(name = "touristAttraction")
public class TouristAttraction {
    @Id
    @GeneratedValue
    @Column(name="touristAttraction_id")
    private Long id;

    private String touristAttractionAttraction;

    private LocalDate touristAttractionStartDate;

    private LocalDate touristAttractionEndDate;

    @ManyToOne
    private Trip trip_d;

    //pusty konstruktor, gettery, settery


    public TouristAttraction() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTouristAttractionAttraction() {
        return touristAttractionAttraction;
    }

    public void setTouristAttractionAttraction(String touristAttractionAttraction) {
        this.touristAttractionAttraction = touristAttractionAttraction;
    }

    public LocalDate getTouristAttractionStartDate() {
        return touristAttractionStartDate;
    }

    public void setTouristAttractionStartDate(LocalDate touristAttractionStartDate) {
        this.touristAttractionStartDate = touristAttractionStartDate;
    }

    public LocalDate getTouristAttractionEndDate() {
        return touristAttractionEndDate;
    }

    public void setTouristAttractionEndDate(LocalDate touristAttractionEndDate) {
        this.touristAttractionEndDate = touristAttractionEndDate;
    }

    public Trip getTrip_d() {
        return trip_d;
    }

    public void setTrip_d(Trip trip_d) {
        this.trip_d = trip_d;
    }
}
