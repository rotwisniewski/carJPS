package controller;

import model.Trip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import repository.TripRepository;

@Controller
@RequestMapping("/addTrip")
public class AddTripController {

    private TripRepository tripRepository;

    @Autowired
    public AddTripController(TripRepository tripRepository) {
        this.tripRepository = tripRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String setupForm(Model model){
        model.addAttribute("trip", new Trip());

        return "addTripForm";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String setupForm(Trip trip){
        tripRepository.save(trip);

        return "home";
    }
}