package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {

    @RequestMapping("/home")
    public String homeHandler(){return "home";}

    @RequestMapping("/tripForm")
    public String formHandler(){
        return "tripForm";
    }

}
